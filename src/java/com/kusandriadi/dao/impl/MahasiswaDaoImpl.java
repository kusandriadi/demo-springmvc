/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kusandriadi.dao.impl;

import com.kusandriadi.dao.MahasiswaDao;
import com.kusandriadi.entity.Mahasiswa;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kus Andriadi
 */
@Repository
@Transactional(readOnly=true, propagation=Propagation.REQUIRED)
public class MahasiswaDaoImpl implements MahasiswaDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional(readOnly=false)
    public void delete(Mahasiswa mahasiswa) {
        sessionFactory.getCurrentSession().delete(mahasiswa);
    }

    public List<Mahasiswa> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Mahasiswa").list();
    }

    public Mahasiswa getNim(String nim) {
        return (Mahasiswa) sessionFactory.getCurrentSession().createQuery("from Mahasiswa where nim = :nim").
                setParameter("nim", nim).uniqueResult();
    }

    @Transactional(readOnly=false)
    public void saveOrUpdate(Mahasiswa mahasiswa) {
        sessionFactory.getCurrentSession().saveOrUpdate(mahasiswa);
    }
}
