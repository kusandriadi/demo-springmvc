/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kusandriadi.dao;

import com.kusandriadi.entity.Mahasiswa;
import java.util.List;

/**
 *
 * @author Kus Andriadi
 */
public interface MahasiswaDao {

    void saveOrUpdate(Mahasiswa mahasiswa);

    void delete(Mahasiswa mahasiswa);

    Mahasiswa getNim(String nim);

    List<Mahasiswa> getAll();
}
